
/**
 *Copyright 2015 Transcosmos Technologic Arts
 *All right reserved.
 */
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tta.demo.entities.Coupon;
import com.tta.demo.services.CouponService;
/**
 * @author denguyen
 *
 */
public class ORMTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("************** BEGINNING PROGRAM **************");
        
        ApplicationContext context = new ClassPathXmlApplicationContext("backend-jpa-context.xml");
        CouponService service = (CouponService) context.getBean(CouponService.class);
        
        List<Coupon> coupons = service.findAll();
        if (null != coupons && !coupons.isEmpty()) {
            System.out.println("Size = " + coupons.size());
            for (Coupon c : coupons) {
                System.out.println("Id = " + c.getId());
                System.out.println("Name = " + c.getName());
                System.out.println("Code = " + c.getCode());
                System.out.println("Start date = " + c.getStartDate());
                System.out.println("Expired date = " + c.getExpiredDate());
                System.out.println("Type = " + c.getCouponType());
                System.out.println("Status = " + c.getStatus());
                System.out.println("=======================================");
            }
        } else {
            System.out.println("Empty!!!");
        }
        
        Coupon newCp = new Coupon();
        newCp.setCode("NEW33333333");
        newCp.setCouponType(2L);
        newCp.setName("NEWHHSCoupon2");
        newCp.setStartDate(new Date());
        newCp.setExpiredDate(new Date());
        newCp.setStatus(1);
        service.save(newCp);

    }

}

package com.tta.demo.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.tta.demo.entities.Coupon;

@Repository
public class CouponDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public int count() {
        return 0;
    }
    public void save(Coupon coupon) {
        getEntityManager().persist(coupon);
    }
    @SuppressWarnings("unchecked")
    public List<Coupon> findAll() {
        return (List<Coupon>)getEntityManager().createQuery("from Coupon").getResultList();
    }
}

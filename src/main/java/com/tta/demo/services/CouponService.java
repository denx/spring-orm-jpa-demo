package com.tta.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tta.demo.daos.CouponDAO;
import com.tta.demo.entities.Coupon;

@Service("couponService")
public class CouponService {
    
    @Autowired
    private CouponDAO couponDAO;
    
    @Transactional
    public void save(Coupon coupon) {
        couponDAO.save(coupon);
    }

    public List<Coupon> findAll() {
        return couponDAO.findAll();
    }

}
